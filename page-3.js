const apiKey = 'rIHbYCAZAhfSFkRImiaHucNi';
const groupID = '4811907';
const cslFileURL = 'https://raw.githubusercontent.com/citation-style-language/styles/master/apa-5th-edition.csl';

const apiUrl = `https://api.zotero.org/groups/${groupID}/collections/LH626869/items?key=${apiKey}&format=bib&style=${cslFileURL}`;

fetch(apiUrl)
    .then(response => {
        updateRequestStatus(response.status, response);
        updateRequestUrl(response.url, response);
        updateRequestType(response.type, response);
        updateRequestMethod;
        return response.text();
    })

    .then(bibliography => {
    // Format the bibliography data
    const formattedBibliography = formatBibliography(bibliography);

    // Display the formatted bibliography in the HTML page
    const bibContainer = document.getElementById('bibContainer');
    bibContainer.innerHTML = formattedBibliography;
    })

    .catch(error => {
        console.error('Error:', error);
        });
    
   
function formatBibliography(bibliography) {
// Add any additional formatting or processing here
return bibliography;
 }


function updateRequestStatus(status) {
 var detailsElement = document.getElementById('request-status');
    console.log('Status:', status)
    detailsElement.innerHTML = 'Status: ' + status;
}
  
function updateRequestUrl(url) {
 var detailsElement = document.getElementById('request-url');
    console.log('URL: ', url)
    detailsElement.innerHTML = 'URL: ' + url;
}
  
function updateRequestMethod(method) {
var detailsElement = document.getElementById('request-method');
    console.log('Method: ', method)
    detailsElement.innerHTML = 'Method: ' + method;
}
  updateRequestMethod('GET')
  
function updateRequestType(type) {
var detailsElement = document.getElementById('request-type');
    console.log('Type: ', type)
    detailsElement.innerHTML = 'Type: ' + type;
}
  
function formatJSON(jsonData) {
return JSON.stringify(jsonData, null, 1)
      .replace(/'/g, '&quot;')
      .replace(/'/g, '&#x27;')
      .replace(/\n/g, '<br>');
}


   