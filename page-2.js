// Funzione per formattare la query GraphQL
function formatGraphQLQuery(graphqlQuery) {
  const queryText = graphqlQuery.query.trim(); // Ottieni la rappresentazione testuale della query GraphQL

  // Applica eventuali formattazioni necessarie solo sulla stringa della query
  return queryText.replace(/\n/g, '<br>'); // Sostituisci i newline con <br>
}

const graphqlQuery = {
  query: `
  query {
    paragraphes(theorie: "critical-code-studies") {
      id
      chapitre
      titre
      theorie
      technique
      citations
      commentaire
      texte {
        id
        contenu
      }
    }
  }
`,
};

function updateRequestStatus(status, response, graphqlQuery) {
  var detailsElement = document.getElementById('request-status');
  console.log("Status:", status);
  detailsElement.innerHTML = 'Status: ' + status;

  var queryElement = document.getElementById('request-query');
  console.log("Query:", graphqlQuery);
  queryElement.innerHTML = 'GraphQL Query:<br>' + formatGraphQLQuery(graphqlQuery);

  return response;
}

function updateRequestUrl(url, response) {
  var detailsElement = document.getElementById('request-url');
  console.log("URL: ", url);
  detailsElement.innerHTML = 'URL: ' + url;

  return response;
}

function updateRequestMethod(method) {
  var detailsElement = document.getElementById('request-method');
  console.log("Method: ", method);
  detailsElement.innerHTML = 'Method: ' + method;
}

function updateRequestType(type, response) {
  var detailsElement = document.getElementById('request-type');
  console.log("Type: ", type);
  detailsElement.innerHTML = 'Type: ' + type;

  return response;
}

function updateRequestData(theorie) {
  var detailsElement = document.getElementById('request-data');
  detailsElement.innerHTML = 'Paragraphs:<br>';

  theorie.forEach((paragraphe) => {
    var paragrapheFormate = formatJSON(paragraphe);
    detailsElement.innerHTML += paragrapheFormate + '<br><br>';
  });
}

function formatJSON(jsonData) {
  return JSON.stringify(jsonData, null, 1)
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#x27;')
    .replace(/\n/g, '<br>');
}


const headers = new Headers();
headers.append('Content-Type', 'application/json');

fetch('https://graphql-ferretti-projet.onrender.com/graphql', {
  method: 'POST',
  headers: headers,
  body: JSON.stringify(graphqlQuery)
})
  .then(response => {
    updateRequestStatus(response.status, response, graphqlQuery);
    updateRequestUrl(response.url, response);
    updateRequestType(response.type, response);
    updateRequestMethod('POST');
    console.log("Response", response);
    return response.json();
  })
  .then(data => {
    if (data.errors) {
      console.error('GraphQL errors:', data.errors);
      throw new Error('GraphQL errors occurred');
    }
    const paragraphes = data.data.paragraphes;
    updateRequestData(paragraphes);
  })
  .catch(error => {
    console.error('Error:', error);
    updateRequestStatus('Error', error);
  });
