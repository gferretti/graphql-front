function updateRequestStatus(status, response) {
    var detailsElement = document.getElementById('request-status');
    console.log("Status:", status);
    detailsElement.innerHTML = 'Status: ' + status;
  
    return response;
  }
  
  function updateRequestUrl(url, response) {
    var detailsElement = document.getElementById('request-url');
    console.log("Source URI:", url);
    detailsElement.innerHTML = 'Source URI: ' + url;
  
    return response;
  }
  
  function updateRequestMethod(method) {
    var detailsElement = document.getElementById('request-method');
    console.log("Method: ", method);
    detailsElement.innerHTML = 'Method: ' + method;
  }
  
  function updateRequestType(type, response) {
    var detailsElement = document.getElementById('request-type');
    console.log("Type: ", type);
    detailsElement.innerHTML = 'Type: ' + type;
  
    return response;
  }
  
  function updateRequestData(paragraphs) {
    var detailsElement = document.getElementById('request-data');
    detailsElement.innerHTML = 'Paragraphs:<br><br>';
  
    paragraphs.forEach((paragraph) => {
      var paragraphText = paragraph.texte.map((item) => item.contenu).join('<br><br>');
      detailsElement.innerHTML += paragraphText + '<br><br>';
    });
  }
  
  function formatJSON(jsonData) {
    return JSON.stringify(jsonData, null, 1)
      .replace(/"/g, '&quot;')
      .replace(/'/g, '&#x27;')
      .replace(/\n/g, '<br><br>');
  }
  
  // Making a GET request
  fetch('http://localhost:3000/paragraphes')
    .then(response => {
      // Update request details when the response is received
      updateRequestStatus(response.status, response);
      updateRequestUrl(response.url, response);
      updateRequestType(response.type, response);
      updateRequestMethod('GET');
      console.log("Response", response);
      return response.json();
    })
    .then(data => {
      // Handle the response JSON data
      updateRequestData(data);
    })
    .catch(error => {
      // Handle any errors
      console.error('Error:', error);
      updateRequestStatus('Error', error);
    });
  