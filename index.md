---
title: Le projet
---


Les services Web sont conçus pour effectuer des opérations spécifiques mises à disposition par d'autres systèmes dans les plus brefs délais et, pour ce faire, ils s'appuient sur une série de protocoles et de langages qui garantissent un échange efficace de données entre client et serveur. Le rôle de ces protocoles et de ces langages reste obscur pour les utilisateurs, qui voient les données requises apparaître à l'écran, sans heurt et en quelques fractions de seconde. Même si elles passent inaperçues, ces technologies structurent les textes et les données qu'ils contiennent, déterminant leur sens [cfr. @vitali-rosatiPourTheorieEditorialisation2020c].<!--ajouter lien avec souchier--> Comment réfléchir à cette activité de modélisation ? Comment observer et rendre visible l'impact des technologies informatiques sur le sens de l'information qu'elles structurent ? 

A travers ce projet et avec un intérêt pour le standard _de-facto_ REST, nous abordons ces questions d'un point de vue technique puis théorique. Nous nous intéressons à REST puisqu'il s'agit de l'un des protocoles les plus utilisés dans les services Web d'aujourd'hui. Il définit donc la structure sur laquelle reposent la
production et la diffusion d’une quantité très importante d’informations et de connaissances.

Nous nous concentrerons en particulier sur la structuration de notre projet de thèse en JSON, un des formats les plus utilisés pour la définition et l'échange de données numériques, couramment utilisé dans les services REST. Cela nous permettra de faire de notre texte une API REST(_Application Programming Interface_), soit une série de points d'accès (_endpoints_) à partir desquels un site web sera alimenté.

L'interface du site sera cohérente avec les hypothèses théoriques du projet. Elle présentera aux utilisateurs certaines données issues des requêtes API, requêtes qui permettront de visualiser le site, ainsi que la structure JSON du projet de thèse. Nous insérons dans l'annexe une première visualisation, pour donner une idée plus concrète du projet. 

La réalisation de ce site nous permettra de nous interroger sur la manière dont le sens de notre texte se modifie au fur et à mesure que la structure de l'API évolue. 
Il ne s'agit toutefois pas d'un projet achevé. En fait, nous souhaitons que l'API nous accompagne et nous guide pendant la rédaction et la conceptualisation de l'ensemble de la thèse, qui porte précisément sur l'impact des protocoles informatiques sur notre vision du monde. 


