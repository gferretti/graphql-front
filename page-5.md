---
title: "Lire le code comme un texte"
bibliography: projet.bib
## js: page-5.js
## API: oui
---

## Pour une herméneutique des protocoles informatiques
### Contexte et méthodologie
#### 1.1 S'intéresser au numérique pour comprendre le présent
Les technologies numériques structurent l’univers dans lequel nous vivons. Elles déterminent temporellement et spatialement la réalité et influencent les représentations, c’est-à-dire notre manière d’appréhender le monde en tant que totalité composée d’objets stables et clairement définis. Pensons, par exemple, aux appareils mobiles.
Ils nous incitent à construire continuellement notre présence en ligne et à diriger notre attention vers la construction d’autres présences, en systématisant une gestuelle «&nbsp;déambulatoire et scripturale&nbsp;» [@cavallariGestesDansEnvironnement2017]. Nous marchons effectivement la tête baissée en tambourinant sur l’écran pour répondre à un message ou nous nous déplaçons dans l’espace en suivant la ligne indiquée par notre navigateur de confiance, en passant nos doigts sur la surface du dispositif pour mieux comprendre où aller. Ces gestuelles jouent un rôle central dans la constitution de notre sphère d’action et de perception, déterminant, notamment, la manière dont nous vivons et comprenons la ville [@cavallariGestesDansEnvironnement2017]. Des dynamiques similaires peuvent être observées dans de nombreux autres contextes&nbsp;: la production et la circulation de l’information, les transactions économiques et la manière dont nous définissons et incarnons les idées de personne et de mémoire sont désormais façonnées et rendues possibles par des technologies informatiques. La confrontation avec ces dispositifs est donc indispensable à toute réflexion sur le présent.

De manière complémentaire, tenter de comprendre le sens de ces dispositifs nous interpelle sur notre
façon de connaître et de vivre dans le monde. La notion de «&nbsp;culture numérique&nbsp;» élaborée par Milad Doueihi théorise précisément cette influence mutuelle, en mettant l’accent sur la portée des dispositifs numériques dans la production et la circulation des connaissances. Il s’agit d’une transformation radicale qui concerne «&nbsp;la nature même des objets de notre savoir&nbsp;» et «&nbsp;l’espace censé les accueillir et les faire circuler&nbsp;» [-@doueihiPourHumanismeNumerique2011a, p. 11].

Pour clarifier cette notion, Doueihi fait référence au concept d’amitié, montrant que la culture
numérique reprend et, en même temps, remodèle profondément la définition classique de cette notion. Il se réfère plus particulièrement aux cas de Facebook et de Wikipédia [-@doueihiPourHumanismeNumerique2011a, p. 81]. D’une part, la définition aristotélicienne d’amitié présuppose une condition d’égalité et d’équivalence entre les sujets, excluant ainsi tout ordre hiérarchique ou relation de pouvoir. D’autre part, alors que Facebook présente une radicalisation du concept, établissant une communauté qui oublie les spécificités et les besoins individuels, Wikipédia exclut toute relation personnelle entre les utilisateurs, identifiant la notion d’égalité
avec l’anonymat des auteurs. Tandis que dans la modélisation de Facebook, le partage est poussé à
l’extrême et concerne tous les domaines, Wikipédia initie une interaction exclusivement tournée vers
le savoir, lui-même identifié à l’information objective.

La culture numérique n'existe pas indépendamment de la culture tout court, la modélisation qu'elle propose en dépend directement. L'exemple sur la notion d'amitié proposé par Doueihi nous montre que les outils numériques sont capables de modifier notre monde parce qu'ils en font essentiellement partie.  

En d’autres termes, l’impact transformateur du dispositif n’est pas une invention créée _ex novo_, mais est également déterminé par les cultures, les pratiques, les institutions, la disponibilité de matériel, les dynamiques spatiales et temporelles au sein desquelles le dispositif lui-même est constitué et exerce son influence. La célèbre expression de Marshall McLuhan« -- «&nbsp;medium is the message&nbsp;» -- [@mcluhanUnderstandingMediaExtensions1994a] pourrait ainsi être intégrée à partir de ces réflexions. Le message dépend de la manière dont le média prend en compte et formate le réel, qui n’est jamais neutre. Le réel peut être transformé par le _medium_, même radicalement, mais ne peut à son tour manquer d’avoir un impact sur la constitution du support lui-même.

---

#### 1.2. Code : entre l'humain et la machine

Le domaine émergent des études critiques du code s’intéresse précisément à l’influence mutuelle
entre le dispositif numérique et les systèmes et cultures dans lesquels il est intégré [@marinoCriticalCodeStudies2020b]. En identifiant dans le code informatique la clé d’accès à l’étude de cette interdépendance, les études
critiques du code cherchent à observer la portée culturelle du code au-delà de la fonction spécifique
pour laquelle il a été créé. Ce dernier est compris en tant qu’artefact, conçu et mis en œuvre dans
un contexte matériel et social. Le code est en effet défini par son histoire, ses usages et par «&nbsp;les interconnexions et les dépendances avec d'autres codes, matériels et cultures&nbsp;»[^1] [-@marinoCriticalCodeStudies2020b p. 31]. 

A partir de ce constat, Mark Marino peut affirmer que l’étude de la structure technique du code en dit long sur le contexte matériel et culturel dans lequel il est conçu et utilisé&nbsp;: «&nbsp;analyzing code to better understand programs and the networks of other programs and humans they interact with, organize, represent, manipulate, transform, and otherwise engage. Reading code functions as an entry point to reading culture&nbsp;» [-@marinoCriticalCodeStudies2020b p. 44]. Si le code est conçu comme un outil épistémologique pour comprendre de telles interactions, sa véracité est garantie par sa nature hybride et médiale. 

En effet, le code est défini par Marino comme le garant de la rencontre entre les intentions humaines, toujours enracinées dans une  culture, et la structure matérielle du dispositif informatique. Il est donc compris en tant qu’expression et concrétisation de la pensée humaine. Concrétisation qui émerge à l’intérieur des outils informatiques en vue de la réalisation d’un programme, d’une fonction ou d’un logiciel spécifique. «&nbsp;[M]ost code is an expression of thought in a kind of shorthand, typically not an ends but a set of symbols that emerge from the process of making something else&nbsp;», écrit encore Marino [-@marinoCriticalCodeStudies2020b, p.234].

Selon cette approche, le code est le support de la pensée humaine dans le contexte technique de la
machine [-@marinoCriticalCodeStudies2020b, p.154]. En tant que symbolisation de la pensée, il devient une représentation du discours humain<!-- et, en même temps, des pratiques et des contraintes imposées par la machine**-->. Marino peut ainsi affirmer la possibilité et la nécessité d’élaborer une lecture herméneutique du code informatique, afin de comprendre les systèmes et les cultures auxquelles il se réfère et qui le déterminent. Il s’agit donc de définir ce qu’il est nécessaire d’observer pour mener à bien une telle analyse herméneutique. 

À la question «&nbsp;What can be interpreted?&nbsp;», Marino répond&nbsp;: «&nbsp;Everything. The code, the documentation,
the comments, the structures, the compiled versions -- all will be open to interpretation&nbsp;», arguant
qu’une lecture du code informatique doit prendre en compte tous ces éléments
[@marinoCriticalCodeStudies2020b, p.44].
Quelques lignes plus loin, ce même auteur soutient l’importance de lire le code en fonction du
destinataire (_audience_) pour lequel il a été écrit. Le destinataire correspond pour Marino à la machine
qui exécute le code, aux autres machines avec lesquelles il interagit et à ceux et celles qui tenteront de comprendre sa structure afin d’y apporter des modifications [@marinoCriticalCodeStudies2020b, p.44]. L'_audience_ garantit l’existence du code en tant qu’objet écrit pour être compris, et mis en œuvre. Outre les intentions et la culture de l’auteur, le code est ainsi également déterminé par les attentes et les capacités de ses lecteurs. 

Parmi ses lecteurs, la machine garantit son exécutabilité, c’est-à-dire sa nature agentive, sa possibilité de faire quelque chose. La nature transformatrice du code en tant qu’incarnation de la pensée est ainsi assurée par sa conformité avec la machine. Les réflexions proposées par les études critiques du code montrent qu’il s’agit d’un artefact culturel intrinsèquement performatif.

Les termes avec lesquels Mark Marino définit le code informatique sont certainement utiles pour
réfléchir sur les instances qui participent à sa production et qui, à leur tour, sont modélisées par le
code lui-même. Cependant, l'assimilation complète de cette dernière à la représentation de la pensée humaine qui veut agir sur la machine pour réaliser autre chose pourrait impliquer des conséquences problématiques. Pour pouvoir remplir ce rôle, le code devrait s'interposer entre deux éléments radicalement différents&nbsp;: la pensée, la culture de l'auteur, et le dispositif sur lequel cette pensée veut agir. Pour remplir cette fonction intermédiaire, le code devrait être un objet modelé par les deux parties, permettant aux deux pôles de communiquer et de se contaminer. C'est ainsi qu'il pourrait constituer la clé de compréhension du dispositif informatique et de la culture humaine que Marino recherchait.

À cet égard, Wendy Chun note que le code est souvent considéré comme l'essence du dispositif numérique parce que la nature de son exécutabilité n'est pas suffisamment problématisé [-@chunSourceryCodeFetish2008]. De son point de vue, établir que le code source est au centre de la communication homme-machine revient en fait à présupposer l'exécutabilité en tant que caractéristique inhérente et innée du code. Une telle présupposition constituerait un stratagème méthodologique pour éviter de problématiser la manière dont son efficacité est garantie. De la négation du problème de l'exécutabilité dépend, selon Chun, l'assimilation fallacieuse du code à la «&nbsp;source&nbsp;», c'est-à-dire à l'objet au cœur du dispositif informatique.

Selon cette perspective, considérer le code comme un intermédiaire entre la culture humaine et le
fonctionnement de la machine revient à poser une relation de cause à effet injustifiée entre les deux
termes. Le code serait l’objet abstrait responsable de cette relation. En reprenant les termes de Chun,
la fonction causale attribuée au code en ferait un «&nbsp;fétiche&nbsp;» ou un «&nbsp;démon&nbsp;», car elle lui conférerait
une forme d’autonomie innée, inexplicable, mais indispensable à l’utilisation et à la compréhension
de tout objet informatique [-@chunSourceryCodeFetish2008].

Ed Finn définit la compréhension courante de l’algorithme en des termes similaires et, en même temps, complémentaires. En effet, il montre que pour le sens commun, l’algorithme est la concrétisation
dans une application ou un programme d’une intelligence informatique autrement incompréhensible
et radicalement différente. Il écrit: «&nbsp;The algorithm becomes the mechanism of translation&nbsp;: the prism or instrument by which the eternally fungible space of effective computability is focalized and instantiated in a particular program, interface, or user experience&nbsp;» [@finnWhatAlgorithmsWant2017, p. 35]. 

À l'instar des remarques de Chun, selon cette perspective, la notion d'algorithme indique un objet intermédiaire, qui renvoie à un sens ultérieur et le concrétise sous une forme donnée. Dans ce cas, le sens symbolisé n'est identifiable ni dans la nature du dispositif, ni dans la pensée humaine, mais précisément dans une intelligence computationnelle, au-delà du dispositif lui-même. L'algorithme concrétiserait cette forme de pensée, la rendant effective dans notre monde matériel et donc intelligible pour nous. Comme si l'algorithme donnait sens au programme. À l'instar du code-fétiche théorisé par Chun, l'algorithme devient l'intermédiaire insondable et imaginé _ad hoc_ pour expliquer une relation causale, qui est en fait problématique. Si le code établit une causalité entre la pensée humaine et la structure matérielle de la machine, la notion d'algorithme décrite par Finn établit une relation similaire entre la pensée et l'intelligence informatique.

Malgré les différences entre les concepts abordés, la difficulté de justifier l’échange d’informations et l’influence mutuelle entre des termes opposés apparaît dans les deux cas. C'est pourtant une influence qui est affirmée par les réflexions citées et qui en est d'ailleurs à l'origine. Le dispositif numérique est composé d’idée et de programme, de besoins humains et de contraintes techniques, de culture et de code -- le code pensé, écrit et exécuté<!--rivedi ultima opposizione-->. Dans l’outil informatique, ces composantes existent «&nbsp;au même niveau&nbsp;», comme le dirait Simondon [@simondonModeExistenceObjets2012, p. 175]. Entendus dans les termes décrits ci-dessus, le code et l'algorithme sont censés garantir cette synergie entre les différents éléments, mais ne parviennent pas à la justifier. Ils mettent donc en évidence la nécessité de la définir plus précisément.

Se référant à la notion de performativité de Judith Butler [cfr. @butlerExcitableSpeechPolitics1997], Chun propose de contourner le problème du support en affirmant la priorité de l'exécution sur le code. Le code émergerait de l'exécution, qui serait caractérisée par une itérabilité rigoureuse [@chunSourceryCodeFetish2008]. À son tour, l'itérabilité performative de l'exécution serait soutenue et rendue possible par une structure institutionnelle, politique et machinique.
Cependant, de cette manière le paradoxe de la communicabilité entre humain et machine risquerait d'être simplement transposé et non résolu. Nous devrions définir la dynamique qui donne naissance à cette structure, car l'omission d'une telle définition laisserait place aux mêmes questions que Chun elle-même avait soulevées en se référant aux risques associés à l'essentialisation du code source. Comme l'algorithme ou le code, une telle structure pourrait en effet incarner une forme de causalité qui n'est pas pleinement justifiée. 

Pour éviter ce danger, il convient de définir une telle structure en recourant aux réflexions du nouveau matérialisme. Il s'agit d'un courant qui s'intéresse au rôle de la matière dans la constitution de la réalité et qui, plus profondément, nie toute opposition ontologique entre le sens et la matière [@gambleWhatNewMaterialism2019a]. La structure décrite par Chun pourrait être assimilée à une telle réalité matérielle.
 
Selon cette perspective, l’intelligence humaine, la pensée logico-formelle, l’algorithme, la structure
des dispositifs techniques, l’exécutabilité qui détermine le code, et les autres éléments que nous
avons mentionnés jusqu’à présent sont des objets et des concepts immanents à la matière, ils en font partie.
Composants d'un même contenu ontologique, ces éléments ne peuvent être pensés qu'en relation et constamment déterminés par cette dernière [@baradMeetingUniverseHalfway2007]. Relation qui, selon le nouveau matérialisme, correspond à l'activité de la matière et à sa reconfiguration continue [@gambleWhatNewMaterialism2019a]. Il ne serait donc plus nécessaire d'imaginer un _medium_ pour l'expliquer.
Afin de comprendre le potentiel de cette approche
pour une analyse herméneutique du dispositif numérique, il est donc nécessaire de se questionner sur la façon dont le dispositif participe à cette relation.

---

#### 1.3. Protocoles informatiques

Il nous semble particulièrement utile d'appliquer cette perspective à l'étude du protocole informatique, car il s'agit de la systématisation de pratiques partagées, résultant d'une négociation entre besoins différents, forces politiques, disponibilités économiques et matérielles. 
Fruit de cette négociation explicite, le protocole établit le modèle et la norme pour faciliter ou optimiser un processus. C'est donc la négociation elle-même qui définit constamment son sens
[@gallowayProtocolHowControl2004a, p. 243].

Le protocole détermine également les processus de production et le développement des compétences qui perpétuent son activité de normalisation, confirmant son influence sur les communautés mêmes qui le mettent en œuvre et contribuent à le définir. Dans le même temps, l’implementation d’un protocole implique la sélection et la systématisation de formats et de langages informatiques à utiliser
dans cette mise en œuvre. Elle déclenche une série de choix et de standardizations qui ne font pas
partie du modèle protocolaire en tant que tel, mais qui sont néanmoins indispensables à son existence
effective. Le protocole en tant que modèle théorique implémenté dans des réalités concrètes nous
fournit ainsi un exemple de l’interdépendance entre systèmes informatiques et culturels.

En prenant l’exemple du protocole TCP/IP, nous examinerons l’articulation de la relationnalité entre les trois objets que nous avons introduits en citant les travaux de Marino et Finn&nbsp;: l’implémentation dans une culture spécifique, la logique formelle et le contexte matériel. Nous nous intéressons plus particulièrement à TCP/IP puisqu’il s’agit du protocole mis en œuvre par Internet ; c’est donc un des systèmes théoriques et fonctionnels qui garantissent la diffusion extensive du numérique dans le monde d’aujourd’hui [@ryanHistoryInternetDigital2010]. Une grande partie de la production et de la circulation de l'information, des transactions économiques et des interactions humaines sont désormais façonnées et rendues possibles par une telle norme. Nous espérons par conséquent que cette étude de cas sera un point de départ pour l'analyse herméneutique d'autres objets numériques et, en général, pour observer l'intégration des technologies numériques dans notre monde.
Dans la thèse achevée, nos réflexions seront également approfondies par une analyse plus précise de HTTP, qui fait partie de la suite de protocoles TCP/IP, et de l'architecture REST, elle-même basée sur HTTP.

---

### Problématique et Hypothèse
<!--RÉSUMÉ : Le chapitre introduit les questions de recherche au cœur du projet de thèse : si la pensée et la matière étaient deux entités ontologiquement distinctes, nous ne pourrions pas comprendre la portée du numérique sur notre monde. En s'appuyant sur le champ disciplinaire du nouveau matérialisme, le chapitre vise à explorer cette difficulté. -->
#### 2.1. Pensée et matière
Les réflexions avancées par Mark Marino sur la signification culturelle du code informatique, par Peppe Cavallari
sur la reconfiguration de l'espace due à l'utilisation de dispositifs numériques, ou par Milad Doueihi sur la notion de culture numérique, nous montrent que les technologies informatiques sont structurées par une sorte de collaboration entre différentes instances, attribuables aux catégories traditionnelles de la pensée et de la matière. Cette relation serait difficilement compréhensible si ces catégories étaient en opposition substantielle. À travers ce projet de thèse, nous voulons montrer la possibilité d'une herméneutique du numérique, qui découle précisément du refus de toute opposition entre la pensée et la matière.

Si l'on admettait l'existence d'une différence ontologique entre ces deux termes, il faudrait en effet soit rejeter la portée singulière du dispositif informatique en le réduisant à une simple puissance de calcul, soit attribuer cette portée à une pensée incohérente avec notre mode de représenter le monde. 

Dans les deux cas, nous n'aurions aucune possibilité de comprendre la portée des objets informatiques.
En effet, si le dispositif numérique était une pensée radicalement différente de la nôtre,
nous ne pourrions le définir que négativement, en soulignant les différences avec notre intelligence.
Nous risquerions sinon d'adopter des définitions métaphoriques, comme l'ont déjà souligné Chun et Finn.
Si, par contre, le dispositif correspondait à un simple substrat matériel, nous devrions penser son impact sur le monde comme notre représentation, qui dépend entièrement de nos facultés d'interprétation.
Nous retomberions alors dans une forme d'idéalisme radical. Le code et l'algorithme
ne seraient que des «&nbsp;langages artificiels&nbsp;», décidés par les seuls humains. Conformément aux observations de Gadamer dans _Vérité et méthode_, ils manqueraient de «&nbsp;vitalité&nbsp;» parce qu'ils n'auraient pas
aucune correspondance avec des objets sensibles [@bajerUnderstandingSourceCode2022c].
Surgirait également le problème de la conciliation entre concept et objet empirique, que Kant tentait déjà de résoudre en introduisant la notion de schème.<!--, e di cui già Bergson evidenziava le criticità_-->

---

#### 2.2. Nouveau matérialisme
Le paradigme du néo-matérialisme nous montre une voie pour repenser l'alternative entre représentation et matière et, par conséquent, pour admettre la possibilité d'une herméneutique de la portée du dispositif informatique. L'adoption d'une telle approche ne peut cependant pas faire l'économie d'une remise en cause partielle d'autres catégories centrales de la tradition de la pensée occidentale, telles que la causalité et le sens même de la compréhension [cfr. @baradPosthumanistPerformativityUnderstanding2003].

Abordons la notion de cause. Nous avons déjà montré que les dispositifs matériels modifient
notre façon de voir le monde et que, dans un sens complémentaire, nos représentations déterminent les technologies informatiques. Excluant donc toute hiérarchisation entre les dispositifs numériques et la pensée, nous devons affirmer une relation de cause entre ces deux objets. Relation qui se veut réciproque et continue. En effet, puisque ces technologies évoluent constamment et que nous ne cessons de les utiliser dans notre vie quotidienne, notre façon de penser change sans cesse avec elles. De même, l'évolution du numérique est dictée par l'évolution de nos besoins, de nos compétences, de la disponibilité des matériaux. Il s'agit d'un conditionnement réciproque et prolongée dans le temps et, plus profondément, d'une détermination mutuelle. La représentation et le dispositif humains découlent donc de cette relation, sans y prendre part en tant qu'entités autonomes et préconstituées [@bozalekCausality2021, p. 40]. Dans ce contexte, la cause et l'effet ne sont pas des propriétés d'éléments distincts mais caractérisent un type d'interdépendance matérielle.
Le concept de cause ainsi repensé s'inscrit dans la notion d'«&nbsp;intra-action&nbsp;» introduite par Karen Barad, selon laquelle l'existence des objets et des concepts est ontologiquement enracinée dans la relationnalité qui les détermine&nbsp;: 

> The neologism "intra-action" signifies the mutual constitution of entangled agencies. That is, in contrast to the usual "interaction," which assumes that there are separate individual agencies that precede their interaction, the notion of intra-action recognizes that distinct agencies do not precede, but rather emerge through, their intra-action. It is important to note that the "distinct" agencies are only distinct in a relational, not an absolute, sense, that is, agencies are only distinct in relation to their mutual entanglement; they don’t exist as individual elements. [@baradMeetingUniverseHalfway2007, p. 33]

Dans ce contexte, la pensée humaine est constitutivement impliquée dans la relation causale avec le dispositif informatique. L'interprétation du numérique ne peut donc s'abstraire de cette relation. 
Au contraire, l'interprétation doit contribuer à déterminer notre représentation et le dispositif numérique. Elle doit être comprise comme faisant partie intégrante de la matière et du processus de sa constitution [@pitts-taylorMatteringFeminismScience2016]. L'herméneutique du numérique ne peut donc pas consister à poser une grille d'interprétation sur le monde, mais doit être un acte. Karen Barad écrit à ce sujet&nbsp;: «&nbsp;knowing, thinking, measuring, theorizing, and observing are material practices of
intra-acting within and as part of the world&nbsp;» [-@baradMeetingUniverseHalfway2007, p. 91]. 

Bien qu'il s'agisse d'une forme de pensée, l'interprétation ne peut être ontologiquement différente de l'action. Elle a une valeur productive, comme l'a écrit Gadamer, bien que dans un contexte différent
[-@gadamerVeriteMethodeGrandes2018]. En tant qu'action
inscrite dans la relation qui détermine l'homme et la machine, l'interprétation reconfigure cette relation et, en même temps, notre façon de la représenter.
Il s'agit d'une «&nbsp;recatégorisation constructive&nbsp;»[^2] de la réalité et d'une modification de celle-ci [@edelmanUniverseConsciousnessHow2001]. La théorie et la pratique, la compréhension et l'action, la représentation et le monde n'existent pas indépendamment les uns des autres.

Cela a au moins deux conséquences. La première est la fin du concept d'identité. Si le monde est
une activité tensionnelle et une reconfiguration continue, il ne peut y avoir d'objets, de personnes ou de concepts
avec une essence définie une fois pour toutes. Ici, le refus d'une coupure radicale
entre représentation et matière implique la contestation d'autres distinctions traditionnellement placées au fondement de la compréhension, telles que celles entre sujet et objet et entre interprète et interprété.

La deuxième conséquence est liée à la première et consiste en la fin de la prérogative humaine sur l'interprétation. Il ne peut y avoir d'interprétation que dans un contexte collaboratif et relationnel, dont l'humain n'est pas le seul acteur. Encore Barad écrit&nbsp;: «&nbsp;In some instances, "nonhumans” (even
beings without brains) emerge as partaking in the world’s active engagement in practices of knowing&nbsp;» [@baradMeetingUniverseHalfway2007, p. 149].

Pour illustrer ce dernier point, revenons à la question de la rigueur du code. Ce ne sont pas nos capacités cognitives qui l'exigent.
Dans notre vie quotidienne, nous nous référons principalement à des informations définies de manière ambiguë et agissons en conséquence. La rigueur nous semble pourtant être une propriété centrale du code, utile pour comprendre sa spécificité. L'intégrité du système matériel qui contribue à le déterminer nous permet également de le comprendre.

Enfin, nous ajoutons que l'interprétation des technologies informatiques devrait tenir compte de la nature relationnelle de l'objet d'étude. Si, pour le néo-matérialisme, les concepts et les
objets sont des conformations particulières de la matière, ces objets doivent être compris à partir du contexte matériel qui leur permet d'exister et dans lequel ils sont ontologiquement intégrés. En prenant en considération le cas mentionné, l'analyse d'un langage informatique, d'un protocole ou d'un algorithme en particulier peut nous renseigner davantage sur les besoins et le système de valeurs incarnés par ces objets<!-- qu'une étude générale du concept de code informatique-->. Essayons d'appliquer ces considérations au sujet du protocole. Décrivons d'abord quelques concepts généraux, qui devront ensuite être clarifiés par l'analyse d'un protocole spécifique, dans notre cas TCP/IP.

<!--Le protocole en tant que concept-->

Nous avons déjà vu que le protocole est le produit d'une série de compétences, d'intérêts, de circonstances sociopolitiques, d'institutions, de relations de pouvoir, d'instruments techniques, de disponibilités matérielles et d'investissements monétaires évolutifs qui se sont croisés à un moment historique donné. Nous pouvons donc affirmer son caractère contingent<!--[@baradMeetingUniverseHalfway2007, p. 149]-->. D'autre part, ces arrangements sont basés sur des modèles computationnels qui, en raison de leur nature formelle, sont nécessairement valides.

---

#### 2.3. Le protocole en tant que concept

Deuxièmement, et toujours en tant qu'objet historiquement déterminé, le protocole informatique est par définition dépassable par les technologies futures. En effet, ce dispositif systématise et modélise des actions destinées à résoudre un ensemble particulier de besoins, en tenant compte d'un éventail défini de technologies. Or, le fait que la structure matérielle du protocole technique organise des actions en vue de certains besoins montre que la configuration même du dispositif systématise et formalise certains besoins, les déclarant valables dans le futur [cfr. @bachimontSensTechniqueNumerique2010].

Pensons, en troisième lieu, à la nature performative du protocole. «&nbsp;Protocol is a circuit, not a sentence&nbsp;», écrit Alexander Galloway [-@gallowayProtocolHowControl2004a, p. 53]. Sa portée dans le monde est assurée par son interaction systémique avec d'autres programmes et dispositifs physiques.
En même temps, nous ne pouvons pas nier la nature stratifiée et radicalement hétérogène des dispositifs informatiques. Nous comprenons, par exemple, comment le code binaire répond à des logiques et à des besoins différents de ceux d'un fichier pdf. Le premier est en effet conçu pour permettre la lecture, l'exécution, par la machine, et le second est constitué pour assurer la lecture humaine.

Ce dernier point nous suggère que les oppositions soulevées jusqu'à présent, loin de montrer la nature contradictoire du dispositif informatique, découlent du fait que ce dernier est composé de différentes instances et logiques, qu'il est nécessaire de sonder en relation avec le fonctionnement de différents objets informatiques. Considérons maintenant ces aspects à la lumière du fonctionnement et de l'histoire de la suite des protocoles TCP/IP qui définit Internet, afin de proposer une subdivision et une abstraction de ses instances constitutives.

---

### Étude de cas et corpus
<!--RÉSUMÉ : Un premier début d'étude de cas est proposé, visant à appliquer les hypothèses formulées précédemment au cas de la suite de protocoles TCP/IP.-->
#### 3.1. L'étude de cas : hypothèses de départ

Pour fournir une première application de ce qui a été dit jusqu'à présent, nous posons trois questions concernant la structure de TCP/IP, qui esquissent trois abstractions possibles pour comprendre le protocole. En nous référant aux études critiques du code, nous avons observé que les dispositifs numériques sont déterminés par un système logico-informatique, par la culture dans laquelle ils sont conçus et mis en œuvre, et par un contexte matériel précis, qui concerne les outils et les compétences disponibles pour une telle mise en œuvre.
En pensant au protocole en particulier, nous avons vu que dans ce système coexistent la revendication d'une validité éternelle et une mutabilité intrinsèque liée aux contingences historiques, une relation nécessaire avec d'autres systèmes et la nécessité de créer un système propre, ce qui détermine l'adoption de certaines technologies plutôt que d'autres.

Premièrement, nous nous interrogeons donc sur la nature des modèles computationnels qui sous-tendent Internet et sur la manière dont ces modèles interagissent avec le cadre historique dans lequel ils sont mis en œuvre.
Dans ce contexte, il convient de noter que le modèle informatique fournit une description logique de la technologie en question. Dans le cas d'internet, cette description concerne le transfert de paquets de données entre client et serveur.

Nous devons ensuite nous demander comment ces models sont liés à la composante physique du protocole et dans quelle mesure cette composante matérielle régule notre champ d'action et s'inscrit dans des systèmes physiques, culturels, spatiaux et cognitifs.
Dans notre cas, nous identifions la composante physique du protocole avec la série de dispositifs, de serveurs, de routeurs, de câbles, d'ondes électromagnétiques qui donnent lieu à la réalité matérielle du réseau.

Enfin, il convient de s'attarder sur la manière dont la mise en œuvre du protocole systématise nos besoins et nos systèmes de valeurs, en devenant partie intégrante de notre culture. 
Nous explicitons ici que la mise en œuvre correspond au moment où le modèle formel et la structure matérielle sont utilisés, adaptés, pour répondre à des besoins humains spécifiques. Dans Internet, ce niveau concerne, par exemple, la construction d'applications et de services, c'est-à-dire la mise en place des systèmes spécifiques de transmission et de traitement des données. Il concerne également la standardisation des techniques et des appareils pour permettre au réseau de subsister en tant que dispositif protocolaire.

---

#### 3.2. Computation

Nous commençons ici à articuler les réponses à ces questions, qui devront être traitées plus en détail dans le cadre de notre thèse.

<!--Computation-->
Abordons la question des modèles computationnels. Ils fournissent une description du monde en termes mathématiques.<!--&nbsp;: quelque chose et rien, 1 et 0, absence ou presence.--> Dans leur sens général, ils sous-tendent une approche qui décrit la complexité (par exemple des systèmes biologiques, informatiques et sociaux) à partir de l'association des unités discrètes par le biais d'un ensemble défini et limité d'opérations logiques, de règles formelles.
Alors que, dans le cas d'autres disciplines, le débat sur la portée ontologique de ces modèles reste ouvert, les systèmes informatiques sont effectivement fondés sur de telles descriptions mathématiques.

Il semble donc évident que les formalismes qui sous-tendent la mise en œuvre des systèmes d'information sont stockés, inscrits dans les systèmes eux-mêmes. Dans le cas d'Internet, il faut cependant ajouter une nuance un peu plus complexe. Internet n'est pas un programme mais une suite de protocoles. Chaque protocole a ses propres fonctions, objectifs et structures et peut être mis en œuvre de différentes manières. Les différents composants de chaque protocole peuvent également construits à partir de modèles mathématiques différents et être spécifiés formellement de manière tout aussi différente.
Dans notre cas, le modèle formel est donc un outil d'écriture qui produit une détermination précise du protocole, c'est-à-dire une spécification de celui-ci ou d'une partie de celui-ci. 

En même temps, la modélisation analytique est aussi un outil interprétatif, herméneutique. Traduire un protocole déjà documenté en un schéma formel qui le représente dans un monde idéal, sans les contraintes de sa mise en œuvre réelle, est utile pour comprendre les protocoles Internet, et donc pour assurer leur meilleure réalisation. Ces analyses formelles sont employées pour explorer les implémentation possibles, les améliorations ou les corrections à apporter aux protocoles ou à leurs fonctionnalités.
L'analytique rend un problème compréhensible parce qu'elle divise son objet en parties manipulables. Ces manipulations, subdivisions, abstractions, spécifient de façon nouvelle ce qu'est Internet.
<!--Dire que les modèles formels ont pour nous une valeur mémorielle, monumentale, semble peut-être audacieux. Pourtant, les applications et les services que nous utilisons quotidiennement dépendent de la répétition de ce type de connaissances.--> 

L'éventail des actions que nous pouvons mettre en œuvre sur un logiciel en ligne est également déterminé par de tels modèles. Le traitement de texte, la conception physique et matérielle de nos appareils, la structuration des langages de programmation (automate déterministe) et de ce qu'on appelle l'intelligence artificielle (grammaire hors contexte), font largement appel à de tels modèles formels, pour ne citer que des exemples tirés du texte de Michael Sipser _Theory of Computation_ [@sipserIntroductionTheoryComputation2012, p. 3]. Ces modèles ont un intérêt performatif, dans le sens où ils contribuent à la façon dont nous écrivons et manipulons le monde par le biais de systèmes informatiques.

Si nous avons évoqué jusqu'à présent l'influence des modèles analytiques sur la configuration d'Internet, nous constatons également que ces modèles sont formulés pour répondre à des besoins déterminés par l'utilisation d'Internet lui-même. Inas Khalifa et Ljiljana Trajkovi, par exemple, illustrent six modélisations possibles du protocole TCP (_Transport Control Protocol_), en les discutant en termes d'évolution [@khalifa2004overview].
Ils montrent comment chacun des six modèles répond à des exigences spécifiques, en l'occurrence liées à la durée de connexion garantie par TCP, sans perte significative de données. Ces modèles diffèrent parce qu'ils incarnent des conceptions et des utilisations différentes du même protocole TCP et du type de connexion qu'il doit garantir. 

D'un point de vue purement logique, les modélisations présentées par Khalifa et Trajkovi sont parfaitement, et donc éternellement, valables. Cependant, elles sont construites à partir d'assomptions qui dépendent de facteurs externes aux modèles eux-mêmes. <!--rivedi meglio questa parte-->Par exemple, le premier des modèles présentés, initialement élaboré par Matthew Mathis, Jeffrey Semke et Jamshid Mahdavi, présume que la quantité de données transmises par le serveur est toujours compatible avec la capacité de réception du client (_window limitation_) [@mathisMacroscopicBehaviorTCP1997]. Dans la même analyse, l'impact sur les temps de reconnexion à la suite d'une interruption due à une latence excessive (_timeout loss_) n'est pas non plus pris en compte<!--riformula-->. L'intérêt, la qualité et la validité des modèles dépendent des conditions physiques et matérielles auxquelles ils se rapportent. Ces conditions et ces modèles analytiques ne constituent pas des systèmes isolés, mais sont directement en relation avec nos actions quotidiennes et, en général, avec l'écosystème dans lequel nous vivons. Nous allons développer ce deuxième point ci-dessous.

---

#### 3.3. Matérialité
La structure physique est en fait l'une des composantes que nous avons identifiée pour définir Internet.
En tant que dispositif étendu dans l'espace, elle régule et unifie les actions humaines et 
la manière dont nous les comprenons et dont nous prévoyons leurs résultats. Comme dans le cas des dispositifs techniques en général, ce niveau d'abstraction nie la dichotomie entre la mémoire et l'écriture, l'idée et le corps. «&nbsp;Le dispositif est la disposition d'éléments dans l’espace, pour commander un déroulement dans le temps&nbsp;», écrit Bruno Bachimont à ce sujet [-@bachimontSensTechniqueNumerique2010, p. 36].

Prenons l'exemple d'un simple câble Ethernet.
Sa disposition dans l'espace implique l'existence d'un dispositif en mesure de s'y connecter. À son tour, l'humain qui interagit avec le câble Ethernet et l’ordinateur répond à une règle déjà inscrite dans la structure physique des appareils eux-mêmes. Cette structure physique détermine la faculté du dispositif de «&nbsp;permettre la répétition d'un geste dont l'efficacité a été prouvée dans le passé en vue d'un résultat donné dans le futur&nbsp;» [@bachimontSensTechniqueNumerique2010, p. 37] <!--**pagina**-->.

Si, comme nous l'avons vu précédemment, nos appareils sont structurés à partir de modèles analytiques formels, ces mêmes appareils prennent également en compte nos attentes, incarnent des réponses à nos besoins, qui sont tout sauf formelles. Prenons un exemple pour expliquer cette idée. Pensons à la diffusion des ports USB-C. Leur popularité est due à leur forme fine, en ligne avec notre besoin de transporter des ordinateurs très légers, et au fait qu'ils ont deux côtés identiques. Cette caractéristique prévient l'usure des portes causée par les erreurs humaines répétées lors de l'insertion des anciennes clés USB.
<!--En lien avec ces thèmes, nous réitérons notre réflexion sur la temporalité du dispositif, que nous avons abordée en citant Bachimont.-->
Les dispositifs tels que les ports USB-C ou les câbles Ethernet nous aident dans notre quotidien tout en organisant nos actions. Pourtant, cela semble évident, ils ne sont pas conçus spécifiquement pour nous en tant que personnes localisées spatialement et temporellement.
Le futur des actions est inscrit dans le fonctionnement du dispositif et est identifiable au résultat que ce dispositif doit nous apporter. Ce futur inscrit se manifestera de temps en temps dans nos actions quotidiennes, mais il restera en fait dans la matière. Il n'est pas lié à la présence d'une entité spécifique, individuelle et corruptible.

Le dispositif décrit ici incarne donc une relation ontologique entre le dispositif lui-même, la structure corporelle humaine, les sociétés dans lesquelles nous nous inscrivons et notre capacité cognitive à s'orienter spatialement et chronologiquement afin d'accomplir le geste ordinaire d'insérer une clé USB-C. De même, l'ensemble des protocoles décrivant Internet prévoient de s'adapter à une variété de besoins humains. Des besoins futurs, mais aussi précisément structurés, décrits, prescrits, donc continuellement actualisés dans le protocole.

Pensons par exemple les formats de fichiers qui sont transmis entre différents appareils grâce à la suite TCP/IP. Les protocoles au niveau des applications et des services anticipent le traitement de ces formats en prévoyant et orientant les types de contenu qu'un être humain pourrait vouloir transmettre sur le réseau. 

D'autre part, il est également vrai que les formats eux-mêmes sont nécessaires à la structuration d'Internet car ils permettent aux appareils connectés au réseau de lire le contenu des documents en code binaire, soit en une succession de signaux électroniques. Ces signaux circuleront entre les différents nœuds du réseau via une série de câbles ou de satellites et de routeurs sous forme d'ondes électromagnétiques. L'infrastructure du réseau imposera à son tour des contraintes supplémentaires sur la taille et la structure des documents transmis.
Les formats des documents circulant sur Internet dépendent également de ces contraintes physiques et contribuent à les façonner.
Nous pouvons en déduire que nos représentations, nos capacités cognitives, les formats, le code binaire et l'infrastructure qui permet son transport façonnent tout autant Internet. TCP/IP est donc loin d'être une entité immatériel.

Le préjugé de la transcendance d'Internet est déconstruit par Nicole Starosielski, autrice du livre _The Undersea Network_ [-@starosielskiUnderseaNetwork2015]. Elle montre que l'existence même du réseau est principalement assurée par des câbles sous-marins et souterrains. En tant qu'objets physiques, la disposition de ces câbles dépend de la présence de structures antérieures utilisées pour les télécommunications, des négociations et des relations géopolitiques entre les États traversés, de la conformation géographique des territoires, et des intérêts gouvernementaux, principalement liés aux questions de sécurité. Bien qu'Internet donne l'impression d'une connexion décentralisée, le nombre limité de câbles traversant les vastes distances des océans centralise le traitement des informations circulant sur le réseau.

---

#### 3.4. Mise en œuvre
Voici que la mise en œuvre d'Internet ne peut que dépendre d'une conformation géographique et géopolitique. Pour approfondir ce point, nous nous intéressons maintenant à la question de l'implémentation.
Il s'agit du moment où la conformation physique de l'appareil et le langage logique convergent pour constituer des services et des réseaux de dispositifs historiquement localisés. Ici, la création de ces services implique la sélection d'un ensemble d'idées, de pratiques et de valeurs. Pour comprendre la nature et la portée politique du protocole, il semble donc opportun d'observer la suite TCP/IP à partir de la situation politique et culturelle dans laquelle ce processus de sélection a débuté. En fait, nous pensons que le triage de valeurs et d'idées effectuée à l'époque se reflète encore en partie aujourd'hui dans la structure matérielle du protocole.

TCP/IP est né pendant la guerre froide, au sein de l'organisation ARPA (_Advanced Research Projects Agency_), créée par le ministère américain en 1958 de la défense pour trouver une réponse adéquate au lancement du Spoutnik par les Soviétiques [@abbateARPANETInternetHistory1994, p. 3]. Il est généralement souligné que la suite de protocoles d'Internet a été mise en œuvre en raison de deux objectifs fondamentaux.  

Le premier serait de permettre l'échange d'informations hautement fiables. La fiabilité est cruciale dans une situation de tensions géopolitiques, où les informations transmises sont extrêmement précises et sensibles.
Ce n'est pas un hasard si le protocole qui garantit le mieux l'intégrité et la fiabilité des informations transmises est TCP (_Transport Control Protocol_), qui est tellement essentiel au fonctionnement du réseau qu'il donne son nom à toute la suite de protocoles. 

Le deuxième objectif de TCP/IP est de permettre à l'information d'atteindre sa destination même en cas d'endommagement d'une partie du réseau concerné. Le protocole par excellence qui garantit l'identification et le suivi des chemins possibles pour la transmission des données entre les composants du réseau est IP (_Internet Protocol_). Une fois encore, compte tenu de l'importance qu'Internet accorde à cette thématique, la suite Internet hérite du nom de ce protocole.

Plus précisément, le suivi garanti par IP permet généralement à un client d'identifier le chemin vers un serveur, l'appareil capable de fournir les données dont il a besoin. Le type d'informations demandées au serveur est spécifié par le client au moyen d'une requête HTTP, qui garantit ainsi l'échange effectif de données, ajoutant une sorte de couche supplémentaire aux protocoles IP et TCP[@shklarWebApplicationArchitecture2009a]. 
L'échange n'a lieu qu'entre deux appareils et est basé sur leur localisation. Il existe donc une forme de contrôle centralisé de cet échange. En ce qui concerne le contexte dans lequel TCP/IP a été conçu, il est plausible que des raisons de sécurité aient motivé son fonctionnement.

Pour prouver que des systèmes alternatifs sont possibles, nous signalons que certains protocoles _peer-to-peer_ ont plus récemment proposé de garantir l'échange d'informations en suivant le contenu (_Content-addressable storage_) et non les localisations des serveurs [@zhongtaoliCANTreeRouting2014, @benetIPFSContentAddressed2014]. Dans le cadre de cette approche, la demande du client ne concerne pas nécessairement un seul dispositif identifié par son adresse IP, mais peut concerner de nombreux appareils non localisés qui disposent des données demandées.

Nous ne pensons pas que la tendance inhérente au contrôle mise en œuvre par l'internet soit entièrement due à une volonté délibérée de traçage. TCP/IP étant l'un des premiers protocoles créés pour connecter des réseaux hétérogènes, il est possible qu'au moment de sa création la prise de conscience des implications de son utilisation n'ait pas encore mûri et que les compétences pour des implémentations alternatives n'aient pas encore été développées [cfr. @abbateARPANETInternetHistory1994]. 

Nous savons également que l'avènement d'ARPANET, première implémentation de TCP/IP, a été accueilli avec beaucoup d'enthousiasme par les entreprises et surtout par la communauté académique américaine. Animé par une vision extrêmement libérale de la recherche, ce projet a financé les idées d'un très large éventail d'ingénieurs, d'universitaires et de développeurs en un temps très court. Nous rapportons à cet égard le témoignage de Charles Herzfeld&nbsp;: «&nbsp;ARPA was the only place in town where somebody could come into my office with a good idea and leave with a million dollars at the end of the day&nbsp;» [@bushPiecesAction1970, p. 137]. <!--Possiamo dunque immaginare che il tema del interessi dei finanziatori e dei ricercatori, più interessati all'implentazione e all'espansione di tecnologie allora all'avanguardia.-->

<!--L'intérêt purement scientifique est également signalé par Mitchel Waldrop dans *The Dream Machine*&nbsp;:

> Why did ARPA build the network?... There were actually two reasons. One was that the network would be good for computer science... This is by far the dominant reason among the researchers. But there was also another side of the story, which was that ARPA was a Defense agency. [-@waldropDreamMachine2018, p. 279]

D'après ce témoignage, les intérêts militaires sont même relégués au second plan par rapport au désir académique et intellectuel de découvrir le potentiel de l'informatique.
Conformément à cette hypothèse, il a été observé que la mise en œuvre d'Internet a été dès le départ ouvertement rendue publique et non gardée secrète [@townesSpreadTCPIP2012]. D’autre part, nous pourrions penser que -- dans ce context -- l'intérêt académique était en accord avec les ambitions de propagande de la partie américaine.--> 
Cultivés dès l'origine par les communautés d'étudiants des plus prestigieuses universités américaines, on peut imaginer que les premiers déploiements et mises en œuvre de l'internet étaient également motivés par un enthousiasme sincère pour l'avancement de la recherche. Dans la succession rapide de nouvelles implémentations et de financements, les intérêts des étudiants, des chercheurs et du gouvernement américain ont trouvé un point de convergence [@oreganBriefHistoryComputing2008, p. 179]. Cependant, l'évolution et l'expansion frénétiques de cette technologie n'ont peut-être pas laissé de place à une véritable remise en question de ses principes fondateurs.

Bien que le sujet de la culture fondatrice de l'Internet mérite d'être approfondi, nous sommes en mesure de tirer quelques conclusions. Le fait que le protocole incarne les valeurs et les intérêts des deux groupes plus ou moins homogènes de personnes nous montre la valeur politique du protocole, qui normalise le discours public en sélectionnant certains modèles sociaux et culturels et en exclut d'autres. 

Ici, au terme de cette analyse, nous observons comment Internet nous fournit une série d'outils herméneutiques pour comprendre et analyser la manière dont l'information que nous voulons communiquer est traitée et traduite par la machine ; comment il nous fournit de nouvelles structurations matérielles de l'espace et du temps, et de nouvelles manières de nous regarder, de nous confronter à notre histoire, et aux principes éthiques et politiques qui déterminent la circulation de l'information en ligne. Voici que une étude attentive des implications structurelles d'Internet pourrait inciter à une réévaluation de celui-ci.

Nous espérons que l’approfondissement du thème du protocole à travers cette étude de cas et le
cadres méthodologiques des études critiques du code et du nouveau matérialisme nous conduira à la
délimitation d’une herméneutique du matériel, fondée à son tour sur les productions théoriques de
notre culture humaniste, qui nous fournit les modèles et les catégories conceptuelles pour vivre et
interpréter le monde.

---

## Bibliographie du projet


[^1]: La traduction est à moi. Texte original&nbsp;: «&nbsp;The source code is a text, and to read it as a cultural object would be to consider it in all its history, uses, and interconnections and dependencies with other code, hardware, and cultures&nbsp;» [@marinoCriticalCodeStudies2020b p. 31].

[^2]: Texte original&nbsp;: «&nbsp;constructive recategorization&nbsp;» [@edelmanUniverseConsciousnessHow2001, p. 95]





<!--pandoc --citeproc --metadata-file metadata.yaml -s --bibliography projet.bib CSDH-yann.md --template eisvogel.tex -o CSDH-yann.pdf
-->












<!--Che dipendono da altre tecnologie implementate. Per esempio, la presenza di limite massimo della quantità di dati che un sender può inviare prima di ricevere un segnale di conferma di ritorno parte del receiver. Questa tecnologia viene chiamata cwnd (_congestion window_) e non -->
<!--A partire dall'esempio di TCP, comprendiamo che Internet non è basato su una serie di modelli formali stabiliti una volta per tutte. Questi infatti evolvono a seconda del mutare delle implementazioni e delle pratiche per cui lo stesso protocollo viene impiegato.--> 
<!--1.  The mo del predicts the bandwidth of a sustained TCP connection sub-
jected to light to mo derate packet losses, such as loss
caused by network congestion. Porta con sé delle assunzioni, per esempio questo modello presuppone l'esistenza della--> 


<!--Si la condition de possibilité de l'herméneutique classique est la disparition de l'auteur, qui laisse l'interprète seul à décrypter le sens de l'objet interprété, l'herméneutique du numérique s'appuie sur la disparition de l'humain comme seul sujet pensant à intervenir dans l'acte de **compréhension**. D'un point de vue épistémologique, cela implique que la représentation du sens commun -- qui divise le monde en sujets et en objets -- n'est pas suffisante pour comprendre le présent. D'un point de vue ontologique, cette affirmation conduit à croire que la pensée est indépendante de l'humain.-->

<!--Pour construire une herméneutique du numérique, il convient alors d'opérer une sélection et une réorganisation des concepts et des outils théoriques dont nous disposons et que nous utilisons et redéfinissons depuis des millénaires pour comprendre le réel.
Ce projet de thèse vise à répondre à ce défi dans le but de définir les conditions de possibilité d'une herméneutique de la matière, c'est-à-dire de la compréhension d'une forme de pensée non-humaine.
Nous développerons ce propos à partir d'une analyse du concept de protocole informatique. Il s'agit en effet d'un objet qui exprime la nature systémique et computationnelle des objets informatiques en général.



> Inglese&nbsp;: Not just occasionally but always, the meaning of a text goes beyond its author. That is why understanding is not merely a reproductive but always a productive activity as well.” -- Verità e Metodo, p. 296 










Per mettere in discussione una tale dualità, non ci resta che ripensare l'autonomia della rappresentazione rispetto al materiale. **In altre parole, per comprendere la portata dei dispositivi informatici dobbiamo mostrare che è solo l'umano a dare senso al mondo**.









rappresentazione non può essere condizione di possibilità dell'esperienza.
- inserimento del neomaterialismo nella tradizione ermeneutica
- alcune conseguenze/specificazioni necessariamente legate a tale inserimento&nbsp;: ridefinizione delle opposizioni tra causa-effetto, soggetto-oggetto -- conoscere/pensiero non è una prerogativa umana.

 Per questo motivo abbiamo scelto di effettuare l'analisi di una suite di protocolli in particolare 

> Si la condition de possibilité de l'herméneutique classique est la disparition de l'auteur, qui laisse l'interprète seul à décrypter le sens de l'objet interprété, l'herméneutique du numérique s'appuie sur la disparition de l'humain comme seul sujet pensant à intervenir dans l'acte de **compréhension**. D'un point de vue épistémologique, cela implique que la représentation du sens commun -- qui divise le monde en sujets et en objets -- n'est pas suffisante pour comprendre le présent. D'un point de vue ontologique, cette affirmation conduit à croire que la pensée est indépendante de l'humain.

Pour construire une herméneutique du numérique, il convient alors d'opérer une sélection et une réorganisation des concepts et des outils théoriques dont nous disposons et que nous utilisons et redéfinissons depuis des millénaires pour comprendre le réel.
Ce projet de thèse vise à répondre à ce défi dans le but de définir les conditions de possibilité d'une herméneutique de la matière, c'est-à-dire de la compréhension d'une forme de pensée non-humaine.




Dobbiamo dunque evidenziare delle tensioni del protocollo. 




 già Bergson evidenziava le criticità e _già al centro dello schematismo kantiano_.

 Già Henri Bergson notava l'icompatibilità tra concetto ed esperienza empirica, nel momento in cui 

Affermare il contrario ci forzerebbe infatti ad immaginare ad hoc dei media di natura ibrida, che garantirebbero in ogni caso tale relazionalità, ma in modo problematico. 

- problemaiticità della traduzione
- problematicità della rappresentazione
- allora&nbsp;: eraclito? filosofia del processo?

È la macchina stessa, come afferma anche Marino, che pone dei vincoli. In altre parole, il codice, l'algoritmo, e gli altri oggetti informatici Tali elementi si conformano anche in corrispondenza all'audience che ne partecipano.  

Se il dispositivo modifica il nostro mondo ed è primariamente in relazione con questo, in che modo possiamo interpretarne il senso? Definire il codice, l'algoritmo o qualsiasi oggetto informatico al pari della rappresentazione del pensiero umano non è sufficiente. Ancor prima . Ma non può essere neppure pensato come qualcosa che sfugge alla rappresentazione, qualcosa autonomo. Ci constringerebbe a fare una filosofia del negativo, come finisce Butler, o, al contrario, finiremmo per delle immagini fantasmi, e utilizzare la parole software etc. per mettere in evidenza qualcosa di ignoto e potente, una sorta di feticcio che Chun. 
Entrambi i casi rappresenterebbero infatti il fallimento dell'ermeneutica. C'è un senso opaco, forse dificcile da comprnedere, ma che l'interpretazione è in grado di dire qualcosa di quest'oggetto. 
Prima di tutto ridefinire il concetto di rappresentazione, ovvero della nostra organizzazione concettuale del mondo. 

Quale approccio adottare per svolgere questo tipo di analisi ermeneutica? Da quali categorie concettuali partire per una tale analisi? In che modo porci nei confronti dell'oggetto studiato, ovvero del dispositivo informatico? 

Per immaginare la possibilità di realizzare questo tipo di analisi ermeneutica, dobbiamo dapprima interrogarci sulle modalità con cui tali dispositivi si determinano ed esistono.

Per compiere questo tipo di analisi dobbiamo dapprima interrogarci sulle modalità con cui i dispositivi si determinano ed esistono e sulla nostra effettiva possibilità di effettuare una lettura ermeneutica di tale modalità
condizioni che determinano l'esistenza di tali dispositivi e sulla nostra effettiva possibilità di effettuarne una lettura ermenutica. [**rendi la frase più specifica**]
Karen Barad scrive ad esempio

Elaborare una metodologia per la comprensione dei dispositivi digitali significa interrogarsi sulla struttura materiale che determina la loro esistenza in quanto oggetti in grado di configurare il mondo. Significa, in altri termini, elaborare un'ermeneutica della materia. Dal momento che l'ermeneutica si interessa del senso, condizione di possibilità per una tale elaborazione è riconoscere la materia come portatrice di senso. [**formula domanda meglio**]
Compito del nostro studio sul protocollo sarà dunque proprio quello di mostrare questo punto, osservando il senso nel protocollo stesso e nelle negoziazioni da cui si determina e a cui dà luogo.
D'altra parte, abbiamo visto che osservare la portata dei dispositivi informatici sul mondo in cui viviamo significa affermare l'intrinseca relazionalità tra cultura umana, pensiero e mondo materiale. Affermare il contrario ci forzerebbe infatti ad immaginare ad hoc dei media di natura ibrida, che garantirebbero in ogni caso tale relazionalità, ma in modo problematico. 
Dal momento che l'ermeneuta si trova già da subito inserito in tale relazionalità, qualsiasi sforzo conoscitivo del materiale non può prescinderne. Tentare di comprendere significa allora partecipare in certa in modalità a tale relazione. In questo senso l'ermeneutica e un'attività e una pratica. Il pensare non ha una natura ontologicamente diversa dall'agire. Scrive a questo proposito Karen Barad «knowing, thinking, measuring, theorizing, and observing are material practices of intra-acting within and as part of the world.» (p.  91). Elaborare un'ermeneutica del materiale significa allora accettare che qualsiasi tentativo ermeneutico è un'azione nel mondo, vi apporterà dunque una modifica. Data l'intrinseca relazionalità di tale azione, l'ermeneuta della materia deve anche accettare che tali modifiche sono delle riconfigurazioni del mondo e del proprio rapporto con il mondo, dunque anche dello stesso interprete. [**riformula**] Nel contesto del neo materialismo ciò implica almeno due conseguenze. La prima, è la fine del concetto di identità. Se il mondo è un'attività tensionale e un continuo riconfigurarsi, non possono esistere oggetti, concetti o persone stabilmente determinati e definiti una volta per tutte. Binomi radicali come quelli tra causa-effetto, soggetto-oggetto, interprete-interpretato devono essere ripensati.
La seconda conseguenza del definire l'ermeneutica come un'attività materiale è legata alla prima e consiste nella fine della prerogativa umana sull'interpretazione. L'ermeneutica della materia deve contemplare un'attività di comprensione non forzatamente umana.
Scrive ancora Karen Barad&nbsp;: «In some instances, ‘‘nonhumans’’ (even beings without brains) emerge as partaking in the world’s active engagement in practices of knowing.» (p. 149)
Per ritornare al problema dell'esecuzione del codice, l'eseguibilità che, secondo Chun, determina la macchina e il codice ci sembra un criterio di comprensione dell'oggetto imposto prevalentemente dalla struttura logica e materiale dei sistemi informatici in cui tale codice deve essere eseguito. 

<!--L'impatto del dispositivo informatico sul mondo sarebbe una nostra rappresentazione del pensiero né una semplice traduzione delle nostre intenzioni in un linguaggio comprensibile alla macchina. _In altre parole,_ la sua struttura non dipende unicamente dalle nostre facoltà<!--, _ma da una particolare conformazione della materia_. Da una parte, identificando l'oggetto informatico con una semplice traduzione, rischiamo di tralasciare il fatto che questo si trova da subito inserito in un sistema tecnico che determina le modalità con cui tale traduzione deve essere effettuata, partecipando alla costituzione dello stesso oggetto. Se, d'altra parte, associamo la stessa tecnologia informatica solo a una rappresentazione propria al pensiero umano, la riduciamo a una concettualizzazione, ricadendo in una forma di costruzionismo o idealismo radicale<!--, che non permetterebbe di comprendere l'autonomia del sistema tecnico. Il codice e l'algoritmo sarebbero dei semplici linguaggi artificiali, decisi a tavolino dall'umano [@bajerUnderstandingSourceCode2022c]. Riemergerebbe dunque il problema della conciliazione tra concetto e oggetto empirico, che già Kant _introducendo_ la nozione di schema sottolineava l'esigenza di chiarire, e di cui già Bergson evidenziava le criticità_ [*rivedi tutto il paragrafo*].



[^1]: Marino riprende questa espressione da @willisCultureMediaLanguage2004

<!--Da un lato, la riflessione di Doueihi sul concetto di amicizia mostra che l'inserimento dei valori classici nel mondo digitale ne determina un rimaneggiamento radicale. Dall'altro lato, le stesse considerazioni indicano che la cultura digitale deve essere definita e compresa a partire da tale rimaneggiamento. In altre parole, i dispositivi informatici conformano il nostro mondo perché ne fanno parte. Il digitale trasforma il nostro mondo non solo perché si è originato al suo interno, ma anche perché essenzialmente non se ne differenzia.  -->



<!--Secondo questa prospettiva, il codice è il risultato e il garante dell'interazione tra istanze diverse, costituite dagli obiettivi e le esigenze di attori e lettori umani e macchinici. Lo studio tecnico dei simboli deve essere dunque accompagnato dall'osservazioni procedure, dalle strutture e dai gesti che vi sono intorno.

Dire che il codice è «concretizzazione» di una forma di pensiero significa affermare che per leggere il codice dobbiamo interrogarci sulla natura di tale pensiero. Le parole di Marino sollecitano dunque a interessarci non solo dei simboli che compongono il codice, ma anche delle «procedures, structures, and gestures» di cui tale codice è la simbolizzazione; delle culture e delle tecnologie che lo determinano [**spiega meglio il doppio movimento del codice che viene determinato e che, allo stesso tempo, determina**]. Ecco che «&nbsp;the study of code requires not only reading the symbols but understanding that those symbols cause changes in state to software and hardware over a duration of time in dynamic engagement with other systems, including humans&nbsp;» (p. 51). Da queste informazioni, ne consegue che il codice 

la réalité universitaire, la façon dont nous nous représentons l'objet "ressource bibliographique" est déterminée par les dispositifs qui la rendent effectivement accessible. En même temps, ces mêmes dispositifs régulent les actions par lesquelles cet objet est distribué et consommé ; ils configurent les compétences et les processus, donc les acteurs et les conditions matérielles, qui participent à cette distribution et à cette utilisation.







Più in generale, il pensare non si differenzia dall'agire.
 deve essere una forma di attività, dunque una pratica, che non può prescindere da tale relazionalità. Barad scrive ad esempio che la comprensione è una forma di intra-azione, .


«practices of knowing are specific material engagements that participate in (re)configuring the world» (p. 91)




Per fare un'ermeneutica del materiale dobbiamo innanzi tutto chiederci che cosa significa

L'interpretazione della materia in quanto attività ci sembra dunque che abbia senso. Ma un senso che ci è comprensibile. Una comprensibilità ontologica

 In che modo, nel nostro tentare di comprendere, entriamo in relazione con le tecnologie?





- Natura computazionale (bachimont) e sistemica di questi oggetti, ma anche empirica... natura diversa. 
- Interagiscono forze con interessi molto diversi
- Mostrare in questa comprensione come la materia abbia il senso
- Il fatto che ci troviamo corrispondere alle logiche umane e computazionali ci fa riflettere su come dovremmo stare al mondo e riflettere il mondo.

- problema della causalità, dello schematismo kantiano. 
- Come comprendere adesso adesso? Comprendere è una prassi. È entrare in relazione con il mondo.
- Come, a partire dall'interazione con il presente, dall'annullamento della conseguenza causale e della distinzione soggetto/oggetto, i concetti della nostra tradizione si mobilizzano e si trasformano






<!--Elaborare una metodologia per la comprensione dei dispositivi digitali è un'operazione complessa non seulement en raison de la portée centrale et de la relative nouveauté de ces dispositifs, mais aussi en raison de la nature computationnelle et, en même temps, systémique de ces objets. Leur nature est en effet incompatible avec la représentation humaine, qui définit le monde comme composé d'objets et non d'unités discrètes, de relations et non de systèmes.

Le fait que le monde d'aujourd'hui soit déterminé par une telle logique computationnelle nous amène à nous interroger non seulement sur notre représentation du monde mais aussi sur ce qui a été dit jusqu'à présent au sujet de la compréhension elle-même. La tradition de l'herméneutique philosophique réfléchit sur l'interprétation en tant que pratique et en tant que méthode, ou -- à partir de Heidegger -- en tant que façon d'être, en sens ontologique. Cette tradition tend à définir la compréhension et la production en tant que facultés, activités, expériences propres à l'humain. L'objet de l'herméneutique est donc conforme au mode de pensée humain. L'interprète est confronté à quelque chose d'inconnu, d'obscur, mais compréhensible. Il se voit en tant que «&nbsp;capable et ayant besoin d'interprétation&nbsp;» [@heidegger_ontologie_2012, p. 24]. 

Pour une herméneutique du numérique, cependant, nous devons nous préoccuper de la logique computationnelle, qui n'est pas humaine, mais matérielle, parce qu'elle est propre à la manière dont les dispositifs informatiques structurent le réel pour répondre aux besoins humains.
Si la condition de possibilité de l'herméneutique classique est la disparition de l'auteur, qui laisse l'interprète seul à décrypter le sens de l'objet interprété, l'herméneutique du numérique s'appuie sur la disparition de l'humain comme seul sujet pensant à intervenir dans l'acte de **compréhension**. D'un point de vue épistémologique, cela implique que la représentation du sens commun -- qui divise le monde en sujets et en objets -- n'est pas suffisante pour comprendre le présent. D'un point de vue ontologique, cette affirmation conduit à croire que la pensée est indépendante de l'humain.

Pour construire une herméneutique du numérique, il convient alors d'opérer une sélection et une réorganisation des concepts et des outils théoriques dont nous disposons et que nous utilisons et redéfinissons depuis des millénaires pour comprendre le réel.
Ce projet de thèse vise à répondre à ce défi dans le but de définir les conditions de possibilité d'une herméneutique de la matière, c'est-à-dire de la compréhension d'une forme de pensée non-humaine.
Nous développerons ce propos à partir d'une analyse du concept de protocole informatique. Il s'agit en effet d'un objet qui exprime la nature systémique et computationnelle des objets informatiques en général.

(décrire une tradition qui oppose humain et non humain mais qui ce n'est pas mon point de départ&nbsp;: questionner une série de opposition + neomaterialism)

## Hypothèse

Avant d'aborder le thème du protocole, il nous semble intéressant d'identifier certaines catégories, certaines tensions dans lesquelles s'inscrivent les dispositifs informatiques, c'est-à-dire les objets et les processus qui incarnent de différentes manières la logique computationnelle et qui, sur la base de cette logique, ont la capacité de façonner le présent. Nous verrons ensuite comment ces concepts peuvent être employés dans une théorie du protocole informatique.

Tout d'abord, en tant que dispositif, l'objet informatique peut être défini par sa nature contingente. En ce sens, on peut affirmer que les technologies et les systèmes informatiques sont le produit d'une série de compétences, d'intérêts, de circonstances sociopolitiques, d'institutions, de relations de pouvoir, d'instruments techniques, de disponibilités matérielles et d'investissements monétaires en évolution qui se sont croisés à un moment historique donné (cf. Barad, p. 147). D'autre part, ces arrangements sont basés sur des modèles mathématiques qui, en raison de leur nature formelle, sont nécessairement valides.

Deuxièmement, et toujours en tant que dispositif technique et historiquement déterminé, l'objet informatique est par définition dépassable par les technologies futures. En effet, le dispositif systématise et modélise des actions destinées à résoudre un ensemble particulier de besoins, en tenant compte d'un ensemble particulier de technologies. Or, le fait que la structure matérielle du dispositif technique organise des actions en vue de certains besoins montre comment la configuration même du dispositif systématise, formalise certains besoins, les déclarant valables dans le futur (cfr. Bachimont).

Pensons, en troisième lieu, à la nature performative du dispositif informatique. «&nbsp;Le protocole est un circuit&nbsp;», écrit Alexander Galloway. Nous pouvons étendre cette affirmation au dispositif en général. Sa portée dans le monde n'est pas déterminée par les programmes, le code informatique, les dispositifs physiques ou les protocoles pris individuellement, mais par l'interaction systématique entre ces éléments. En même temps, nous ne pouvons pas nier la nature stratifiée et radicalement hétérogène des dispositifs informatiques. Nous comprenons, par exemple, comment le code binaire répond à des logiques et à des besoins différents de ceux d'un fichier pdf. Le premier est en effet conçu pour permettre la lecture, l'exécution, par la machine, et le second est constitué pour assurer la lecture humaine.

Ce dernier point nous suggère que les oppositions soulevées jusqu'à présent, loin de montrer la nature contradictoire du dispositif informatique, découlent du fait que ce dernier est composé de différentes instances et logiques, qu'il est nécessaire de sonder en relation avec le fonctionnement de différents objets informatiques. Considérons maintenant le protocole TCP/IP qui définisse Internet afin de proposer une division, une abstraction des instances qui le composent.

### Étude de cas et corpus

 Nous en identifions ici trois&nbsp;:

1. le modèle computationnel, qui fournit une description logique/formelle de la technologie en question. Dans le cas d'Internet, il fournit une description formelle de la manière dont un ensemble de données, de paquets, est transmis d'un ordinateur, d'un hôte, à un autre.

2. la structure matérielle, la composition physique de la technologie en question. Dans notre cas, il s'agit de la série de dispositifs, de serveurs, de routeurs, de câbles (Ethernet ??), d'ondes électromagnétiques (WiFi) qui donnent lieu à la réalité matérielle du réseau.


3. le niveau de la mise en œuvre, qui correspond au moment où le modèle formel et la structure matérielle sont utilisés, adaptés, pour répondre à des besoins humains spécifiques. Dans l'Internet, ce niveau concerne, par exemple, la construction d'applications et de services, c'est-à-dire l'établissement d'une transmission de données particulière et son traitement approprié. Mais il concerne aussi la standardisation des techniques et des dispositifs pour permettre au réseau de subsister en tant que système de transmission de données entre différents dispositifs.

Voyons maintenant pourquoi chacune de ces trois composantes concerne la mémoire.

- Le cas du modèle informatique est peut-être le plus évident. Il s'agit d'une approche qui décrit le monde par le biais d'unités discrètes, minimales et distinctes les unes des autres. Ces unités ne peuvent être caractérisées que de deux manières&nbsp;: quelque chose et rien, 1 et 0, absence ou presence.
Dans son sens général, cette approche décrit la complexité (par exemple des systèmes biologiques, informatiques et sociaux) à partir de l'association de ces unités par le biais d'un ensemble défini et limité d'opérations logiques, de règles formelles.

Alors que, dans le cas d'autres disciplines, le débat sur la portée ontologique de ces modèles reste ouvert, les systèmes informatiques sont effectivement fondés sur de telles descriptions mathématiques.

Il semble donc évident que les formalismes qui sous-tendent la mise en œuvre des systèmes d'information sont stockés, inscrits dans les systèmes eux-mêmes. Dans le cas d'Internet, il faut cependant ajouter une nuance un peu plus complexe que la simplicité de cette affirmation. L'Internet n'est pas un programme mais une *suite* de protocoles. Chaque protocole a ses propres fonctions, objectifs et structures et peut être mis en œuvre de différentes manières. Sans oublier que les différents composants de chaque protocole peuvent être construits à partir de divers modèles mathématiques et spécifiés formellement de manières tout aussi différentes. 
Dans notre cas, le modèle formel est donc un outil d'écriture qui produit une détermination précise du protocole, c'est-à-dire une spécification de celui-ci ou d'une partie de celui-ci. 
En même temps, la modélisation formelle est aussi un outil interprétatif, herméneutique. Traduire un protocole déjà documenté en un schéma formel qui le représente dans un monde idéal, sans les contraintes de sa mise en œuvre réelle, est une technique d'analyse utilisée pour comprendre les protocoles Internet. Cette technique est couramment utilisée pour explorer les extensions possibles, les améliorations ou les corrections à apporter à ces protocoles ou à leurs fonctionnalités.
L’analyse mathématique rend un problème compréhensible parce qu'elle divise l'objet de ce problème en parties manipulables. Ces manipulations, subdivisions, abstractions, spécifient de façon nouvelle ce qu'est Internet.
Dire que les modèles formels ont pour nous une valeur mémorielle, monumentale, semble peut-être audacieux. Pourtant, les applications et les services que nous utilisons quotidiennement dépendent de la répétition de ce type de connaissances. L'éventail des actions que je peux mettre en œuvre sur un logiciel en ligne est également déterminé par de tels modèles. Le traitement de texte, la conception physique et matérielle de nos appareils (automatisation déterministe), la structuration des langages de programmation et de ce qu'on appelle l'intelligence artificielle (grammaire sans contexte), font largement appel à de tels modèles formels, pour ne citer que des exemples tirés du texte de Michael Sipser *Introduction to the Theory of Computation* (p. 3). Ces modèles ont un intérêt performatif, dans le sens où ils contribuent à la façon dont nous écrivons et manipulons le monde par le biais de systèmes informatiques. 

- Passons maintenant à la composante matérielle, la deuxième caractéristique que nous avons indiquée pour définir la structure d'Internet. Comme le monument, la structure physique d’Internet régule et unifie les actions humaines et la prédiction du résultat de ces actions. Comme dans le cas des dispositifs techniques en général, ce niveau d'abstraction nie la dichotomie entre la mémoire et l'écriture, l'idée et le corps. Je cite Bruno Bachimont qui écrit&nbsp;:
"Le dispositif est la disposition d'éléments dans l’espace, pour commander un déroulement dans le temps".
 La disposition spatiale d'un câble Ethernet, par exemple, implique l’existence d’un dispositif capable de se connecter à ce câble. À son tour, l'humain qui interagit avec le câble Ethernet et l’ordinateur répond à une règle déjà inscrite dans la structure physique des appareils eux-mêmes. Cette structure physique détermine la capacité du dispositif – capacité soulignée par Bachimont --de «&nbsp;permettre la répétition d'un geste dont l'efficacité a été prouvée dans le passé en vue d'un résultat donné dans le futur&nbsp;». 
Si, comme nous l'avons vu précédemment, nos appareils sont structurés à partir de modèles mathématiques formels, ces mêmes appareils prennent également en compte nos attentes,  incarnent des réponses à nos besoins, qui sont tout sauf formelles. Prenons un exemple banal pour expliquer cette idée, pensons à la diffusion des ports USB-C. Leur popularité est due à leur forme fine, en ligne avec notre besoin de transporter des ordinateurs très légers, et au fait qu'ils ont deux côtés identiques. Cette caractéristique prévient l'usure du port causée par les erreurs humaines répétées lors de l'insertion des anciennes clés USB.
En lien avec ces thèmes, nous réitérons notre réflexion sur la temporalité du dispositif, que nous avons abordée en citant Bachimont.
Les dispositifs tels que les ports USB-C ou les câbles Ethernet nous aident dans notre quotidien tout en organisant nos actions. Pourtant, cela semble évident, ils ne sont pas conçus spécifiquement pour nous en tant que personnes localisées spatialement et temporellement.
Le futur est inscrit dans le fonctionnement du dispositif et est identifiable au résultat que ce dispositif doit nous apporter. Ce futur inscrit se manifestera de temps en temps dans mes actions quotidiennes, mais il restera en fait dans la matière, il n'est pas lié à ma présence en tant qu'être individuel et corruptible. Le dispositif incarne donc une relation ontologique a-temporelle entre le dispositif lui-même, la structure corporelle humaine et la capacité cognitive de l’humain à s'orienter spatialement et chronologiquement afin d'accomplir le geste banal d'insérer une clé USB. De même, l'ensemble des protocoles décrivant l'internet prévoient de s'adapter à une variété de besoins humains. Des besoins futurs, mais aussi précisément structurés, décrits, prescrits, donc éternellement actualisés dans le protocole.
Pensons par exemple aux formats de fichiers qui sont transmis entre différents appareils grâce à la suite TCP/IP. Les protocoles au niveau des applications et des services anticipent le traitement de ces formats en prévoyant les types de contenu qu'un être humain pourrait vouloir transmettre sur le réseau. D'autre part, les formats eux-mêmes sont nécessaires à la structure d'Internet car ils permettent aux appareils qui le peuplent de lire le contenu dans leur propre langage, le code binaire.
Les protocoles fournissent donc des principes généraux pour garantir que cette traduction entre le code binaire et le langage naturel se déroule conformément aux besoins de la machine et de l’humain. 

- Nous avons maintenant atteint le dernier des trois niveaux énumérés au début de cette présentation&nbsp;: l'implémentation, c'est-à-dire le moment où le matériel et le langage formel convergent pour donner naissance à un service, un réseau de dispositifs historiquement localisés. Les besoins et les valeurs humains que le protocole en tant que monument met en œuvre sont sélectionnés à partir d'une série de négociations, de discussions, de ressources, de relations de pouvoir. Ces négociations ont eu lieu à un moment donné dans le temps et l'espace. Observer la suite TCP/IP à partir de sa relation (ontologique) avec la situation politico-culturelle à ce moment-là nous semble essentiel pour comprendre la nature et la portée politique du protocole. 

TCP/IP est né à l'époque de la guerre froide, au sein du ministère américain de la défense, plus précisément au sein de l'organisation ARPA (Advanced Research Projects Agency). L'ARPA était elle-même le fruit des efforts nord-américains pour trouver une réponse adéquate au lancement du Spoutnik par les Soviétiques. Il est généralement souligné que la suite de protocoles de l'Internet a été mise en œuvre en raison de deux objectifs fondamentaux&nbsp;: 
1. d'une part, permettre l'échange d'informations hautement fiables. La fiabilité est essentielle dans un état de guerre, où les informations transmises sont extrêmement précises et sensibles.
Ce n'est pas un hasard si le protocole qui garantit par excellence l'intégrité et la fiabilité des informations transmises est le TCP (Transport Control Protocol).
2. D'autre part, le deuxième objectif de la suite TCP/IP serait de permettre à l'information d'atteindre la destination souhaitée même si une partie du réseau est corrompue, par exemple par une attaque extérieure. Là encore, ce n'est pas un hasard si le protocole par excellence pour tracer les chemins de transmission des données entre les différents appareils qui peuplent le réseau est le protocole Internet (IP).
Cependant, les témoignages de ceux qui ont travaillé au développement du réseau durant ces années prennent une autre direction. Par example, Mitchel Waldrop dans *The Dream Machine* écrit&nbsp;:

> Why did ARPA build the network?... There were actually two reasons. One was that the network would be good for computer science... This is by far the dominant reason among the researchers. But there was also another side of the story, which was that ARPA was a Defense agency. (p. 279)

Dans ce cas, intérêts militaires sont relégués au second plan par rapport au désir académique et intellectuel de découvrir le potentiel de l'informatique. Conformément à cette hypothèse, il a été observé que la mise en œuvre de l'Internet a été dès le départ ouvertement rendue publique et non gardée secrète (citant, par exemple, l'universitaire américain Miles Townes -- 2012). D’autre part, nous pourrions penser que -- dans ce context -- l'intérêt académique était en accord avec les ambitions de propagande de la partie américaine. En tout état de cause, le fait que le protocole incarne les valeurs et les intérêts d'un groupe plus ou moins homogène de personnes nous présente la valeur politique du protocole, la normalisation du discours public opérée par la norme, qui sélectionne certains modèles sociaux et culturels et en exclut par conséquent d'autres.

Ici, au terme de cette analyse, nous observons comment Internet nous fournit une série d'outils herméneutiques pour comprendre et analyser la manière dont l'information que nous voulons communiquer est traitée et traduite par la machine ; comment il nous fournit de nouvelles structurations matérielles de l'espace et du temps, et de nouvelles manières de nous regarder, de nous confronter à notre histoire, et aux principes éthiques et politiques qui déterminent la circulation de l'information en ligne.

### Utilité pour la recherche

Nous espérons que l'approfondissement du thème du protocole à travers ce cadre méthodologique et l'analyse des la suite TCP/IP nous conduira à la délimitation d’une herméneutique du matériel, fondée à son tour sur les productions théoriques de notre culture humaniste, qui nous fournit les modèles et les catégories conceptuelles pour vivre et interpréter le monde.





- [x] mercoledì 17 maggio&nbsp;: lista bibliografica + fine stato dell'arte + idee problematica
- [ ] giovedì 18 maggio&nbsp;: problematica
- [ ] venerdì 19 maggio&nbsp;: problematica
- [ ] sabato 20 maggio&nbsp;: problematica 
- [ ] domenica 21 progetto&nbsp;: allunga altri passaggi -- aggiungi informazioni specifiche/tecnice sul protocollo.
idee progetto tecnico + presentazione CSDH (pianifica entrambe le proposizioni e scrivi a Yann)







-----

### Corpus

Afin d'explorer ce qui a été dit jusqu'à présent, cette thèse analysera le fonctionnement de TCP/IP, la suite de protocoles qui composent l'Internet. C'est en effet l'ensemble des règles et des institutions qui définissent la plupart de nos activités quotidiennes, à commencer par la production et la circulation de l'information sur le réseau. Alexander Galloway souligne à cet égard son aspiration intrinsèquement totalitaire. L’auteur nous montre que le protocole est le résultat et non la condition préalable d’un «&nbsp;comportement distribué&nbsp;», anti-hiérarchique et autorégulé, qui, par sa nature accueillante et flexible, le protocole Internet assume un pouvoir universaliste (2004, 82). Ce dernier décrit le monde selon ses catégories, il en élabore une cartographie particulière, le structure en le contrôlant. Le protocole apparaît ainsi dans sa portée tant ontologique que politique. Il sera intéressant d'observer à cet égard la divergence entre une compréhension possible du protocole Internet en tant qu'institution, organe d'autorité, infrastructure monumentale de notre époque, et le modèle distribué mis en évidence par Galloway.

### Méthodologie

Nous espérons avoir montré jusqu'ici que pour élaborer une compréhension du protocole et, en général, de toute technologie informatique, il est nécessaire de discuter de certains concepts au cœur de toute la culture occidentale. 
Pour faire une herméneutique de la matière, qui étudie le fonctionnement et la portée du dispositif informatique, les thèmes de la matière, de l'écriture, de la technique, du texte, du dispositif, de l'humain, de la nécessité formelle, du temps et de la mémoire sont mobilisés.
Il est alors nécessaire d'établir un cadre méthodologique pour nous guider dans l'utilisation de ces outils théoriques. Ainsi, nous nous référerons d'abord à la tradition herméneutique et à ses réflexions sur la compréhension et sur l'objet d'un tel acte.
Nous traiterons ensuite du texte et de l'écriture à partir de la «&nbsp;rupture&nbsp;» poststructuraliste (Archibald 2009, 36) et, en particulier, de la définition du texte donnée par Jacques Derrida (1993). Ce positionnement postructuraliste a établi la priorité généalogique et hiérarchique de l’écriture, du texte, par rapport à la parole humaine. Cette approche nous permet d'imaginer la possibilité d'un texte indépendant de notre activité et de notre pensée. Plus radicalement, pour montrer que le texte en tant que tissu est matériel, je fais référence à la ligne de pensée érigée par les penseur·euse·s du nouveau matérialisme (Gamble, Hanan, et Nail 2019). Enfin, en m’inscrivant dans le paradigme méthodologique proposé par les études critiques du code, je montrerai combien cette textualité matérielle porte en elle des modèles, des valeurs, des cultures qui disent quelque chose de nous et qui, en même temps, nous transforment (Marino 2020). 

=======

### Utilité pour la recherche

Nous espérons que l'approfondissement du thème du protocole à travers ce cadre méthodologique et l'analyse des la suite TCP/IP nous conduira à la délimitation d’une herméneutique du matériel, fondée à son tour sur les productions théoriques de notre culture humaniste, qui nous fournit les modèles et les catégories conceptuelles pour vivre et interpréter le monde.


- Indexation. cfr. Susan Brown
- A Companion to DH -- A New Companion
- Interface effect
- Alan Mil, pratiques du protocol numérique
- Doueihi
- History of Internet&nbsp;: The Missing Narratives
- le chatons&nbsp;: serveur -- low tech -- reconstruir mon internet&nbsp;: questionner la norme de ces protocol !
- Robert Caillau&nbsp;: «&nbsp;Moi, j'ai toujours rêvé d'une république de citoyens responsables, mais où est elle? Les géants du Web sont des impérialistes qu'on distingue à peine des États totalitaires, qui décident de ce qui est acceptable ou non, Le Web, c'est Facebook et du commercial, rien d'autre. Je ne veux plus y aller&nbsp;» ~ Robert Cailliau
- The undersea network

---
Linguaggio come casa dell'essere&nbsp;: l'uomo dialogico
- ermeneutica&nbsp;: Gadamer
- comunità ereditata dalla comunicazione&nbsp;: Apel e Habermas
- rapporto con l'altro come volto rivolto all'infinito

Ermes&nbsp;: arte dell'interpretazione
All'inizio: regole per pensare certi tipi di testi. ex. ermeneutica biblica, giuridica, letteraria...
Tuttavia nel 900 ermeneutica significato più generale, ermeneutica proprio come filosofia generale: pensa che il fenomeno dell'interpretazione non è solo un fenomeno che riguarda il rapporto nostro con certi testi, ma riguarda tutta l'esistenza di tutti i giorni. Quando ci accostiamo al mondo non siamo una tabula rasa, il mondo ci appare già come qualcosa di articolato&nbsp;: per fare esperienza del mondo dobbiamo disporre di certi _schemi_ che ci dirigono. E questi schemi li ereditiamo con il linguaggio naturale. Certamente non lo assimiliamo sulla base di un rispecchiamento oggettivo di fatto, ma il lungaggio è la nostra prospettiva, il nostro equipaggiamento per approcciarci al mondo.
Non c'è esperienza del mondo se non disponendo di un _linguaggio_. La conoscenza è interpretazione se non come rispecchiamento oggettivo. Qualunque rapporto con il mondo è un rapporto linguistico. Si tratta di un patrimonio che mi serve, se non ci fossi io che conosco, che mi faccio delle domande, non conoscerei nulla.

Verità e metodo&nbsp;: come se fosse un titolo polemico, vs metodo delle scienze positive. Verità come interpretazione e non come rispecchiamento del mondo. Ma anche le domande scientifiche sono già cariche di teorie (ermeneutica&nbsp;: io che arrivo portando delle conoscenza, degli strumenti... e il mondo mi risponde sulla base delle domande che gli faccio) -- premesse che non si riducono all'oggetto che viene descritto.

Ciò comporta la liberazione delle scienze dello spirito da quel sospetto che gravava su di esse rispetto alle scienze della natura (diciamo che anche le sicenze della natura sono in qualche modo interpretative)

La stessa nozione di verità cambia, se la verità è intepretazione: mettersi d'accordo, individuare una condizione soddisfacente di un dialogo, piuttosto che rispecchiare un dato oggettivo -- verità anche nell'esperienze dell'arte. Faccio esperienza di verità quando faccio l'esperienza che mi cambia -- _il mio mondo si modifica come un risultato di un dialogo_
Il modello della verità significa l'esperienza del dialogo, che ci fornisce anche un modello morale&nbsp;: costituire continuamente l'intesa con gli altri. La verità è l'accordo tra me e il testo, tra me e l'altro: devo cercare di capire e interpretare in modo soddisfacente (concordata tra i due) -- fusione d'orizzonti. cfr. new materialism&nbsp;: anche in questo caso il mio modo di interpretare cambia il mondo e non è mai una visione a partire da una tabula rasa.

Nell'ermeneutica come nel nuovo materialismo mettersi in discussione, significa non solo cambiare alcune nozioni principali, ma precisamente cambiare il nostro mondo e il modo con cui lo interpretiamo. -- in entrambi i casi è una trattativa per decidere del mondo.

## Parti cancellate




<!--Su questo aspetto, pur con le debite differenze, la postura di Marino e delle tendenze descritte da Chun e Finn si avvicinano alla ricerca della convergenza tra memoria umana e mondo macchinico che Simondon ascriveva all'origine del fenomeno del «&nbsp;couplage&nbsp;». Il couplage simondoniano descrive infatti il rapporto sinergico tra questi due mondi. Rapporto che presuppone la scoperta da parte dell'umano di un « codage commun » entre sa mémoire, son sens, et celui produit par la machine, au point che egli arriva ad observer une « convertibilité partielle » entre les deux [@simondonModeExistenceObjets2012, p. 172]. I due oggetti partecipano della stessa relazione. Similmente alla teoria dei critical code studies, Simondon si interessa del paradosso della comunicabilità tra l'umano e la macchina, i due sistemi che il codice o l'algoritmo in quanto media avrebbero il compito di mettere in comunicazione. Si tratta del paradosso della convergenza tra idea e esecuzione, pensiero e programmazione, che i dispositivi informatici, così corrispodenti ai bisogni e alle tendenze umane, sembrano suggerire. 
Se, per Finn, l'umano è ossessionato dalla ricerca di pattern algoritmici perché vuole scoprire il senso della computazionalità oltre l'algoritmo, dove la computazionalità è intesa come una forma di intelligenza efficacissima e incomprensibiunale, dunque magica, per Marino il senso del codice permane difficile da cogliere perché dipende dalla molteplicità dei sistemi e delle culture in cui si trova inserito. In entrambi i casi, l'elemento informatico rimanda ad un senso ulteriore. Marino si interessa della cultura in cui è inserito l'umano e, insieme, della stratificazione tecnica in cui si inscrive il codice. Per Finn, d'altro canto, l'umano si interessa di qualcosa perché crede all'esistenza del senso vero della computazione, un concetto metaforico, che indica una forma di intelligenza incomprensibile. Il codice come algoritmo sono simboli, sono tracce di qualcosa che l'umano può vedere solo attraverso il filtro del codice. Sono oggetti intermedi, che effettuano una traduzione, una mediazione linguistica e culturale tra oggetti essenzialmente differenti. Il problema dell'esecuzione sollevato da Chun è, in fin dei conti, il problema della comunicabilità tra due oggetti posti in fondamentale opposizione. -->

<!--Riassumendo quanto detto fino ad ora, abbiamo visto come il codice per Marino sia un'entità intermediaria, che emerge dalla volontà dell'umano di interagire con la macchina. Il codice fungerebbe dunque da medium tra la cultura umana e il sistema di tecnologie in cui egli si inscrive. Nell'opinione comune descritta da Finn, anche l'algoritmo sarebbe un medium, questa volta tra un programma o un servizio specifico e il senso sottinteso all'algoritmo, una forma di intelligenza computazionale più profonda dell'algoritmo stesso e diversa da quella umana. Per comprendere come le istanze descritta da Marino e da Finn interagiscono tra loro determinando il nostro rapporto con i dispositivi digitali, ci sembra utile riprendere e riformulare la struttura materiale immaginata da Chun, a partire dalle riflessioni del nuovo materialismo, che si interessa del ruolo attivo della materia nella costituzione del reale e, più profondamente, nega qualsiasi contrapposizione tra senso e materia (Gamble et Al). -->

<!--Per definire i generici concetti di codice, di algoritmo e, più in generale, di dispositivo informatico, è allora essenziale interrogarci sulla natura del nostro oggetto di studio, cioè sul contesto in cui i singoli script, linguaggi informatici, programmi, algoritmi e protocolli si configurano, come tra l'altro aveva già puntualizzato Marino relativamente al codice [**ritrova citazione**]. La relazionalità causale tra cultura umana, pensiero logico-formale e oggetto informatico fino ad ora osservata cesserebbe in questo modo di essere problematica. Se considerati come tre oggetti indipendenti, la loro relazionalità sarebbe difficile da spiegare e domanderebbe di interrogarci sulla natura del medium in grado di garantire tale relazionalità. Se, come nel caso del nuovo materialismo, questi tre elementi sono pensati in quanto configurazioni particolari della stessa materia, la relazione rimarrebbe intrinseca.
Questo secondo caso, ci porta a un ripensamento della nozione di causalità, di cui già Hume notava le criticità. Escluderebbe infatti l'esistenza di una causa e di un effeto pensati in quanto oggetti singolari e totalmente distinti. Comprendere un dispositivo informatico significherebbe comprendere a partire da quali dinamiche questo si costituisce e di quali dinamiche prende a sua volta parte.-->
<!--
- Dici di Galloway: che descrive e prescrive il reale. Dice che cosa è buono e che cosa non è buono. Che cosa può esistere e che cosa non può. Il protocollo è un circuito, non una sentenza, un sistema di pratiche che descrive e prescrive e che, per funzionare, deve coinvolgere le altre stratificazioni del computer, dall'infrastruttura materiale, al modello formale, al codice, ai formati. In altre altre parle, cita sempre Galloway, deve contenere altre negozioni, decisioni, pratiche formalizzate, dunque altri standard. Scrive infatti Kittler "il protocollo contiene sempre altri protocolli".
 
- Ermeneutica: utile perché rinuncia nega la verità univoca e compresa una volta per tutte. Ma il senso in continua rielaborazione.


<!-- Questione dello schema&nbsp;: tra oggetto empirico e concetto. -->
